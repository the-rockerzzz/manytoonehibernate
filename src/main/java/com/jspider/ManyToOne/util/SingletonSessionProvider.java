package com.jspider.ManyToOne.util;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

/**
 * @author Saptarshi Das
 *
 */
public class SingletonSessionProvider {
	private static Session ses;
	/**
	 *@param getSession
	 * @return Session object
	 */
	public static Session getSession() {
		if(ses==null) {
			ses=new Configuration().configure().buildSessionFactory().openSession();
			return ses;
		}
		else return ses;
	}
}
