package com.jspider.ManyToOne.constant;

/**
 * @author Saptarshi Das
 *
 */
public interface AppConstant {
	public static String STUDENT_INFO="student";
	public static String TEACHER_INFO="teacher";
}
